# NTHU Biomedical Image Analysis Final Project

### Brain Tumor IDH Status Classification

This project addresses the classification of IDH mutation status of brain tumor, which is seen as an important task in the past few years. Given an MRI image as input, we build a simple system that can classify the tumor is either in LGG or HGG level. Focusing especially on the segmented images are also taken as information, since we also construct a segmentation network, that able to provide extra features about the tumor. That is, there two sub-network in our model: the segmentation network and the classifier. We propose a novel framework to generate segmentation and output the prediction. The two sub-network are trained collaboratively to make use of the interaction information. The results of our project prove the effective support by the segment information and provide an acceptable accuracy by an uncomplicated model structure.

### Mainly use tools
Keras, Python

### For more information, please checkout Team_2.pdf
